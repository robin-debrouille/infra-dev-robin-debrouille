# Information de déploiement de Robin Débrouille
### doker
## install des containers
```bash
cd docker
docker-compose up --build
docker-compose exec dev-d7-robin-debrouille bash
```

## install de drupal
```bash
cd ../
rm -R html/
drush make makefile/robin-debrouille.dev.make.ini html
drush site-install robin_debrouille_profile --account-name=robin --account-pass=debrouille --db-url=mysql://root:root@mysql-robin-debrouille/robin-debrouille --site-name="Robin Debrouille"
drush rd-ica
```