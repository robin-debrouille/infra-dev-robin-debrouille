FROM php:7.2-apache

#installation
RUN apt-get update && apt-get install --no-install-recommends --allow-unauthenticated -y \
    mysql-client \
    zip \
    curl \
    imagemagick \
    libcurl4-openssl-dev \
    libfreetype6-dev \
    libjpeg-turbo-progs \
    libjpeg62-turbo-dev \
    libmcrypt-dev \
    libpng-dev \
    libxml2-dev \
    libcurl4-openssl-dev \
    ssmtp

RUN docker-php-ext-configure gd \
      --with-freetype-dir=/usr/lib/x86_64-linux-gnu/ \
      --with-jpeg-dir=/usr/lib/x86_64-linux-gnu/

RUN docker-php-ext-install exif mysqli opcache pcntl pdo_mysql gd zip


RUN apt-get clean && apt-get autoremove -q

#config
ADD config/error_high_level_php.ini /usr/local/etc/php/conf.d/conf-custom.ini
RUN a2enmod deflate expires headers mime rewrite \
&& echo "<Directory /var/www/html>\nAllowOverride All\n</Directory>" > /etc/apache2/conf-enabled/allowoverride.conf
ADD config/site-enable.conf /etc/apache2/sites-enabled/000-default.conf