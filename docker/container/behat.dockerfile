FROM php:7.2-cli

RUN apt-get update && apt-get install -y \
    git \
    vim \
    unzip \
    wget \
    mysql-client \
    colordiff \
    zip \
    curl \
    sudo \
    imagemagick \
    locales \
    libcurl4-openssl-dev \
    libfreetype6-dev \
    libjpeg-turbo-progs \
    libjpeg62-turbo-dev \
    libmcrypt-dev \
    libpng-dev \
    libxml2-dev \
    ruby-sass \
    libcurl4-openssl-dev

RUN docker-php-ext-install exif mysqli opcache pcntl pdo_mysql gd zip

RUN apt-get clean && apt-get autoremove -q

RUN  curl https://getcomposer.org/download/1.6.3/composer.phar -sLo /usr/local/bin/composer \
     && chmod +x /usr/local/bin/composer

#config php
RUN echo "date.timezone='Europe/Paris'\nmemory_limit = -1\n" > /usr/local/etc/php/conf.d/conf-custom.ini

#config lang
RUN sed -i -e "s/# fr_FR.*/fr_FR.UTF-8 UTF-8/" /etc/locale.gen && \
    dpkg-reconfigure --frontend=noninteractive locales
ENV LANG fr_FR.UTF-8
    
#install behat

ADD config/behat/composer.json /opt/drupalextension/composer.json

RUN cd /opt/drupalextension/ \
    && composer install \
    && ln -s /opt/drupalextension/bin/behat /usr/local/bin/behat

#config user
RUN useradd -G sudo user && echo "user:user" | chpasswd
ENV PATH=${PATH}:/home/user/bin
USER user

WORKDIR /var/www/html
