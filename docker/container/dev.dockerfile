FROM php:7.2-cli

RUN apt-get update && apt-get install -y \
    git \
    vim \
    unzip \
    wget \
    mysql-client \
    colordiff \
    zip \
    curl \
    sudo \
    imagemagick \
    locales \
    libcurl4-openssl-dev \
    libfreetype6-dev \
    libjpeg-turbo-progs \
    libjpeg62-turbo-dev \
    libmcrypt-dev \
    libpng-dev \
    libxml2-dev \
    ruby-sass \
    libcurl4-openssl-dev

RUN docker-php-ext-install -j$(nproc) iconv exif mysqli opcache pcntl pdo_mysql gd zip \
&&  docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
&& docker-php-ext-install -j$(nproc) gd

RUN apt-get clean && apt-get autoremove -q

RUN curl https://github.com/drush-ops/drush/releases/download/8.1.16/drush.phar -sLo /usr/local/bin/drush \
    && curl https://drupalconsole.com/installer -sLo /usr/local/bin/console-drupal \
    && curl https://getcomposer.org/download/1.6.3/composer.phar -sLo /usr/local/bin/composer \
    && chmod +x /usr/local/bin/console-drupal /usr/local/bin/drush /usr/local/bin/composer

#config php
RUN echo "date.timezone='Europe/Paris'\nmemory_limit = -1\n" > /usr/local/etc/php/conf.d/conf-custom.ini

#config lang
RUN sed -i -e "s/# fr_FR.*/fr_FR.UTF-8 UTF-8/" /etc/locale.gen && \
    dpkg-reconfigure --frontend=noninteractive locales
ENV LANG fr_FR.UTF-8
    
ENV COMPOSER_HOME /composer
ENV PATH /composer/vendor/bin:$PATH
ENV COMPOSER_ALLOW_SUPERUSER 1
RUN composer global require drupal/coder && \
composer global require dealerdirect/phpcodesniffer-composer-installer && \
PATH=${PATH}:/composer/vendor/bin/

#config user
RUN useradd -G sudo user && echo "user:user" | chpasswd
ENV PATH=${PATH}:/home/user/bin:/var/www/html/vendor/bin
USER user



WORKDIR /var/www/html
