FROM node:8.12-jessie

RUN npm install -g gulp
RUN npm install -g bower

WORKDIR /var/local
